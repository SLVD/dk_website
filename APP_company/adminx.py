from django.contrib import admin
import xadmin
# Register your models here.
from . import models

xadmin.site.register(models.Company_banner)
xadmin.site.register(models.Company)
xadmin.site.register(models.Innovation)
xadmin.site.register(models.Layout)
xadmin.site.register(models.Honor)

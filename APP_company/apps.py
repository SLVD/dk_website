from django.apps import AppConfig


class AppCompanyConfig(AppConfig):
    name = 'APP_company'
    verbose_name = '关于我们'

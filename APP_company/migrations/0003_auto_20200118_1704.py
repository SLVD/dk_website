# Generated by Django 2.2.3 on 2020-01-18 17:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('APP_company', '0002_auto_20200107_2216'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='img',
            field=models.ImageField(blank=True, null=True, upload_to='solution'),
        ),
        migrations.AlterField(
            model_name='company_banner',
            name='img',
            field=models.ImageField(blank=True, null=True, upload_to='solution'),
        ),
        migrations.AlterField(
            model_name='honor',
            name='img',
            field=models.ImageField(blank=True, null=True, upload_to='solution'),
        ),
        migrations.AlterField(
            model_name='innovation',
            name='img',
            field=models.ImageField(blank=True, null=True, upload_to='solution'),
        ),
        migrations.AlterField(
            model_name='layout',
            name='img',
            field=models.ImageField(blank=True, null=True, upload_to='solution'),
        ),
    ]

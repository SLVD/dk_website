# Generated by Django 2.2.3 on 2020-01-18 18:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('APP_company', '0003_auto_20200118_1704'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='info',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='company_banner',
            name='info',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='honor',
            name='info',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='innovation',
            name='info',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='layout',
            name='info',
            field=models.TextField(blank=True, null=True),
        ),
    ]

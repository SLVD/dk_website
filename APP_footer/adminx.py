from django.contrib import admin
import xadmin
# Register your models here.
from . import models

xadmin.site.register(models.Footer)
xadmin.site.register(models.Web_info)

# Generated by Django 2.2.3 on 2020-01-18 18:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('APP_footer', '0002_auto_20200118_1712'),
    ]

    operations = [
        migrations.AlterField(
            model_name='web_info',
            name='info',
            field=models.TextField(blank=True, null=True),
        ),
    ]

from django.db import models

# Create your models here.


# 网站信息管理
class Footer(models.Model):
    name = models.CharField(max_length=128, blank=True, null=True)
    tel = models.CharField(max_length=128, blank=True, null=True)
    email = models.CharField(max_length=128, blank=True, null=True)
    address = models.CharField(max_length=128, blank=True, null=True)
    copy = models.CharField(max_length=128, blank=True, null=True)
    weixin = models.ImageField(upload_to='footer', blank=True, null=True)
    status = models.BooleanField(default=True)

    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    update_time = models.DateTimeField(verbose_name='更新时间', auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        # db_table = 't_solution_banner'
        verbose_name = '网站信息管理'
        verbose_name_plural = verbose_name


# 网站描述
class Web_info(models.Model):
    name = models.CharField(max_length=256, blank=True, null=True)
    keyword = models.CharField(max_length=256, blank=True, null=True)
    info = models.TextField(blank=True, null=True)

    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    update_time = models.DateTimeField(verbose_name='更新时间', auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        # db_table = 't_solution_banner'
        verbose_name = '网站描述'
        verbose_name_plural = verbose_name

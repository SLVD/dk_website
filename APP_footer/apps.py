from django.apps import AppConfig


class AppFooterConfig(AppConfig):
    name = 'APP_footer'
    verbose_name = '网站信息管理'

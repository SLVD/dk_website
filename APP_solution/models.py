from django.db import models

# Create your models here.


# 解决方案 banner
from APP_header.models import PageDetail


class Solution_banner(models.Model):
    img = models.ImageField(upload_to='solution', blank=True, null=True)
    url = models.ForeignKey(
        to=PageDetail,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        verbose_name='链接地址',
        db_constraint=False)
    title = models.CharField(max_length=128, blank=True, null=True)
    info = models.TextField(blank=True, null=True)
    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    update_time = models.DateTimeField(verbose_name='更新时间', auto_now=True)
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    class Meta:
        # db_table = 't_solution_banner'
        verbose_name = '解决方案 banner'
        verbose_name_plural = verbose_name


# 方案背景
class Solution(models.Model):
    img = models.ImageField(upload_to='solution', blank=True, null=True)
    url = models.ForeignKey(
        to=PageDetail,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        verbose_name='链接地址',
        db_constraint=False)
    title = models.CharField(max_length=128, blank=True, null=True)
    info = models.TextField(blank=True, null=True)
    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    update_time = models.DateTimeField(verbose_name='更新时间', auto_now=True)
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    class Meta:
        # db_table = 't_solution'
        verbose_name = '方案背景'
        verbose_name_plural = verbose_name


# 方案概述
class Sol_summarize(models.Model):
    img = models.ImageField(upload_to='solution', blank=True, null=True)
    url = models.ForeignKey(
        to=PageDetail,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        verbose_name='链接地址',
        db_constraint=False)
    title = models.CharField(max_length=128, blank=True, null=True)
    info = models.TextField(blank=True, null=True)
    sort = models.IntegerField(default=0, verbose_name='图片排序')
    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    update_time = models.DateTimeField(verbose_name='更新时间', auto_now=True)
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    class Meta:
        # db_table = 't_sol_summarize'
        verbose_name = '方案概述'
        verbose_name_plural = verbose_name


# 方案优势
class Sol_advantage(models.Model):
    img = models.ImageField(upload_to='solution', blank=True, null=True)
    url = models.ForeignKey(
        to=PageDetail,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        verbose_name='链接地址',
        db_constraint=False)
    title = models.CharField(max_length=128, blank=True, null=True)
    info = models.TextField(blank=True, null=True)
    sort = models.IntegerField(default=0, verbose_name='图片排序')
    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    update_time = models.DateTimeField(verbose_name='更新时间', auto_now=True)
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    class Meta:
        # db_table = 't_sol_advantage'
        verbose_name = '方案优势'
        verbose_name_plural = verbose_name


# 应用场景
class Sol_apply(models.Model):
    img = models.ImageField(upload_to='solution', blank=True, null=True)
    url = models.ForeignKey(
        to=PageDetail,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        verbose_name='链接地址',
        db_constraint=False)
    title = models.CharField(max_length=128, blank=True, null=True)
    info = models.TextField(blank=True, null=True)
    sort = models.IntegerField(default=0, verbose_name='图片排序')
    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    update_time = models.DateTimeField(verbose_name='更新时间', auto_now=True)
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    class Meta:
        # db_table = 't_sol_apply'
        verbose_name = '应用场景'
        verbose_name_plural = verbose_name


# 案例
class Sol_case(models.Model):
    img = models.ImageField(upload_to='solution', blank=True, null=True)
    url = models.ForeignKey(
        to=PageDetail,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        verbose_name='链接地址',
        db_constraint=False)
    title = models.CharField(max_length=128, blank=True, null=True)
    info = models.TextField(blank=True, null=True)
    sort = models.IntegerField(default=0, verbose_name='图片排序')
    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    update_time = models.DateTimeField(verbose_name='更新时间', auto_now=True)
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    class Meta:
        # db_table = 't_sol_case'
        verbose_name = '案例'
        verbose_name_plural = verbose_name


# 相关产品
class Solution_relation(models.Model):
    img = models.ImageField(upload_to='solution', blank=True, null=True)
    url = models.ForeignKey(
        to=PageDetail,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        verbose_name='链接地址',
        db_constraint=False)
    title = models.CharField(max_length=128, blank=True, null=True)
    info = models.TextField(blank=True, null=True)
    sort = models.IntegerField(default=0, verbose_name='图片排序')
    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    update_time = models.DateTimeField(verbose_name='更新时间', auto_now=True)
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    class Meta:
        # db_table = 't_solution_relation'
        verbose_name = '相关产品'
        verbose_name_plural = verbose_name

from django import template
from APP_header.models import *
from APP_footer.models import *

register = template.Library()


@register.inclusion_tag('footer.html')
def footer():
    context = {}
    header = []
    data_list = Header_admin.objects.filter(status=1, pid=None).order_by('sort')
    for _o in data_list:
        header.append({
            "url": _o.url.url,
            "title": _o.title,
        })
    footer = {}
    data_list = Footer.objects.filter(status=1)
    for _o in data_list:
        footer.update({
            "name": _o.name,
            "tel": _o.tel,
            "email": _o.email,
            "address": _o.address,
            "copy": _o.copy,
            "weixin": '/media/' + str(_o.weixin)
        })
    context['header'] = header
    context['footer'] = footer
    return context

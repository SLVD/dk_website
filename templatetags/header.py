from django import template
from django.forms import model_to_dict

from APP_header.models import *
from APP_website.serializers import HeaderSerializer

register = template.Library()


@register.inclusion_tag('header.html')
def header():
    context = {}
    # context['header'] = header()

    header = []
    data_list = Header_admin.objects.filter(status=1, pid=None).order_by('sort')
    for _o in data_list:
        subMenu = []
        qs = Header_admin.objects.filter(status=1, pid_id=_o.id)
        if data_list:
            for __o in qs:
                subMenu.append({
                    "title": __o.title,
                    "url": __o.url.url
                })
        header.append({
            "url": _o.url.url,
            "title": _o.title,
            "sort": _o.sort,
            "subMenu": subMenu
        })
    context['header'] = header
    return context

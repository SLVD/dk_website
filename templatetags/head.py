from django import template
from APP_footer.models import *
from django.forms import model_to_dict

register = template.Library()


@register.inclusion_tag('head.html')
def head():
    head = {}
    data = Web_info.objects.filter().first()
    if data:
        head = model_to_dict(data)
    # head = {
    #     "name": data.name,
    #     "keyword": data.keyword,
    #     "info": data.info
    # }
    return head

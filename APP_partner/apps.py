from django.apps import AppConfig


class AppPartnerConfig(AppConfig):
    name = 'APP_partner'
    verbose_name = '合作伙伴'

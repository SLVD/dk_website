from django.contrib import admin
import xadmin
# Register your models here.
from . import models

xadmin.site.register(models.Partner_banner)
xadmin.site.register(models.Strategic)
xadmin.site.register(models.Channel)
xadmin.site.register(models.Process)
xadmin.site.register(models.Interests)

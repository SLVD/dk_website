# Generated by Django 2.2.3 on 2020-01-07 22:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('APP_partner', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='channel',
            name='url',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.SET_NULL, to='APP_header.PageDetail', verbose_name='链接地址'),
        ),
        migrations.AlterField(
            model_name='interests',
            name='url',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.SET_NULL, to='APP_header.PageDetail', verbose_name='链接地址'),
        ),
        migrations.AlterField(
            model_name='partner_banner',
            name='url',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.SET_NULL, to='APP_header.PageDetail', verbose_name='链接地址'),
        ),
        migrations.AlterField(
            model_name='process',
            name='url',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.SET_NULL, to='APP_header.PageDetail', verbose_name='链接地址'),
        ),
        migrations.AlterField(
            model_name='strategic',
            name='url',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.SET_NULL, to='APP_header.PageDetail', verbose_name='链接地址'),
        ),
    ]

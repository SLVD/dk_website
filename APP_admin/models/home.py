
from django.db import models

"""
class Swiper(models.Model):
        img = models.ImageField(upload_to='media')
        url = models.CharField(max_length=255, blank=True, null=True)
        title = models.CharField(max_length=128, blank=True, null=True)
        sub_title = models.CharField(max_length=128, blank=True, null=True)
        sort = models.IntegerField(default=0, verbose_name='图片排序')
        create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
        update_time = models.DateTimeField(verbose_name='更新时间', auto_now=True)
        status = models.BooleanField(default=True)

        class Meta:
                db_table = 't_swiper'


class Company(models.Model):
        img = models.CharField(max_length=255, blank=True, null=True)
        url = models.CharField(max_length=255, blank=True, null=True)
        title = models.CharField(max_length=128, blank=True, null=True)
        info = models.CharField(max_length=128, blank=True, null=True)
        create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
        update_time = models.DateTimeField(verbose_name='更新时间', auto_now=True)
        status = models.BooleanField(default=True)

        class Meta:
                db_table = 't_company'


class Service(models.Model):
        img = models.CharField(max_length=255, blank=True, null=True)
        url = models.CharField(max_length=255, blank=True, null=True)
        title = models.CharField(max_length=128, blank=True, null=True)
        info = models.CharField(max_length=128, blank=True, null=True)
        sort = models.IntegerField(default=0, verbose_name='图片排序')
        create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
        update_time = models.DateTimeField(verbose_name='更新时间', auto_now=True)
        status = models.BooleanField(default=True)

        class Meta:
                db_table = 't_service'


class Client(models.Model):
        img = models.CharField(max_length=255, blank=True, null=True)
        url = models.CharField(max_length=255, blank=True, null=True)
        title = models.CharField(max_length=128, blank=True, null=True)
        info = models.CharField(max_length=128, blank=True, null=True)
        sort = models.IntegerField(default=0, verbose_name='图片排序')
        create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
        update_time = models.DateTimeField(verbose_name='更新时间', auto_now=True)
        status = models.BooleanField(default=True)

        class Meta:
                db_table = 't_client'
"""

import json

'''
from django.forms import model_to_dict
from basic.basic_response import DataPackage, jsonp_wrapped_response, jsonp_res_data
from basic.basic_response_result import MSG, CODE
from APP_admin.serializers import *

def swiper_list(request):
    if request.method == 'GET':
        data_list = Swiper.objects.filter(status=1).order_by('sort')
        ser = SwiperSerializer(data_list, many=True)
        data_package = DataPackage().set_elements(list(ser.data))
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T'], data=data_package))


def company_list(request):
    if request.method == 'GET':
        data = Company.objects.filter(status=1).first()
        print('da', data)
        ser = CompanySerializer(data)
        print(ser.data)
        data_package = DataPackage().set_fields(dict(ser.data))
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T'], data=data_package))


def service_list(request):
    if request.method == 'GET':
        data_list = Service.objects.filter(status=1).order_by('sort')
        ser = ServiceSerializer(data_list, many=True)
        data_package = DataPackage().set_elements(list(ser.data))
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T'], data=data_package))


def client_list(request):
    if request.method == 'GET':
        data_list = Client.objects.filter(status=1).order_by('sort')
        ser = ClientSerializer(data_list, many=True)
        data_package = DataPackage().set_elements(list(ser.data))
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T'], data=data_package))


def swiper_insert(request):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        obj = Swiper.objects.create(
            img=data_post['img'],
            url=data_post['url'],
            sort=data_post['sort'],
            title=data_post['title'],
            sub_title=data_post['sub_title'],
        )
        data_package = DataPackage().set_fields({
            'obj': model_to_dict(obj)
        })
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T'], data=data_package))


def company_insert(request):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        obj = Company.objects.create(
            img=data_post['img'],
            url=data_post['url'],
            title=data_post['title'],
            info=data_post['info'],
        )
        data_package = DataPackage().set_fields({
            'obj': model_to_dict(obj)
        })
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T'], data=data_package))


def service_insert(request):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        obj = Service.objects.create(
            img=data_post['img'],
            url=data_post['url'],
            title=data_post['title'],
            info=data_post['info'],
            sort=data_post['sort'],
        )
        data_package = DataPackage().set_fields({
            'obj': model_to_dict(obj)
        })
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T'], data=data_package))


def client_insert(request):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        obj = Client.objects.create(
            img=data_post['img'],
            url=data_post['url'],
            title=data_post['title'],
            info=data_post['info'],
            sort=data_post['sort'],
        )
        data_package = DataPackage().set_fields({
            'obj': model_to_dict(obj)
        })
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T'], data=data_package))


def swiper_update(request, id):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        print('post', data_post)
        print(id)
        Swiper.objects.filter(id=id).update(
            img=data_post['img'],
            url=data_post['url'],
            sort=data_post['sort'],
            title=data_post['title'],
            sub_title=data_post['sub_title'],
        )
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


def company_update(request, id):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        Company.objects.filter(id=id).update(
            img=data_post['img'],
            url=data_post['url'],
            title=data_post['title'],
            info=data_post['info'],
        )
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


def service_update(request, id):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        Service.objects.filter(id=id).update(
            img=data_post['img'],
            url=data_post['url'],
            title=data_post['title'],
            info=data_post['info'],
            sort=data_post['sort'],
        )
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


def client_update(request, id):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        Client.objects.filter(id=id).update(
            img=data_post['img'],
            url=data_post['url'],
            title=data_post['title'],
            info=data_post['info'],
            sort=data_post['sort'],
        )
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


# 发布 | 撤回
def swiper_option(request, id):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        Swiper.objects.filter(id=id).update(
            status=data_post['status']
        )
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


def service_option(request, id):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        Service.objects.filter(id=id).update(
            status=data_post['status']
        )
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


def client_option(request, id):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        Client.objects.filter(id=id).update(
            status=data_post['status']
        )
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


def swiper_delete(request, id):
    if request.method == 'POST':
        Swiper.objects.filter(id=id).delete()
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


def service_delete(request, id):
    if request.method == 'POST':
        Service.objects.filter(id=id).delete()
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


def client_delete(request, id):
    if request.method == 'POST':
        Client.objects.filter(id=id).delete()
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))
'''

from django.urls import path, re_path
from .views import *

"""
urlpatterns = [
    
    # ========== 首页 ==========
    # 主页swiper
    path('index/swiper/list/', swiper_list, name='swiper_list'),
    path('index/swiper/insert/', swiper_insert, name='swiper_insert'),
    path('index/swiper/update/<int:id>/', swiper_update, name='swiper_update'),
    path('index/swiper/option/<int:id>/', swiper_option, name='swiper_option'),
    path('index/swiper/delete/<int:id>/', swiper_delete, name='swiper_delete'),
    # 公司简介
    path('index/company/list/', company_list, name='company_list'),
    path('index/company/insert/', company_insert, name='company_insert'),
    path('index/company/update/<int:id>/', company_update, name='company_update'),
    # 核心产品

    # 行业服务
    path('index/service/list/', service_list, name='service_list'),
    path('index/service/insert/', service_insert, name='service_insert'),
    path('index/service/update/<int:id>/', service_update, name='service_update'),
    path('index/service/option/<int:id>/', service_option, name='service_option'),
    path('index/service/delete/<int:id>/', service_delete, name='service_delete'),
    # 我们的客户
    path('index/client/list/', client_list, name='client_list'),
    path('index/client/insert/', client_insert, name='client_insert'),
    path('index/client/update/<int:id>/', client_update, name='client_update'),
    path('index/client/option/<int:id>/', client_option, name='client_option'),
    path('index/client/delete/<int:id>/', client_delete, name='client_delete'),

    # ========== 产品 ==========
    # 产品banner
    path('product/banner/list/', product_banner_list, name='product_banner_list'),
    path('product/banner/insert/', product_banner_insert, name='product_banner_insert'),
    path('product/banner/update/<int:id>/', product_banner_update, name='product_banner_update'),
    # 产品概述
    path('product/product/list/', product_list, name='product_list'),
    path('product/product/insert/', product_insert, name='product_insert'),
    path('product/product/update/<int:id>/', product_update, name='product_update'),
    path('product/product/option/<int:id>/', product_option, name='product_option'),
    # 产品功能
    path('product/function/list/', function_list, name='function_list'),
    path('product/function/insert/', function_insert, name='function_insert'),
    path('product/function/update/<int:id>/', function_update, name='function_update'),
    path('product/function/option/<int:id>/', function_option, name='function_option'),
    path('product/function/delete/<int:id>/', function_delete, name='function_delete'),
    # 产品优势
    path('product/advantage/list/', advantage_list, name='advantage_list'),
    path('product/advantage/insert/', advantage_insert, name='advantage_insert'),
    path('product/advantage/update/<int:id>/', advantage_update, name='advantage_update'),
    path('product/advantage/option/<int:id>/', advantage_option, name='advantage_option'),
    path('product/advantage/delete/<int:id>/', advantage_delete, name='advantage_delete'),
    # 应用场景
    path('product/apply/list/', apply_list, name='apply_list'),
    path('product/apply/insert/', apply_insert, name='apply_insert'),
    path('product/apply/update/<int:id>/', apply_update, name='apply_update'),
    path('product/apply/option/<int:id>/', apply_option, name='apply_option'),
    path('product/apply/delete/<int:id>/', apply_delete, name='apply_delete'),
    # 案例
    path('product/case/list/', case_list, name='case_list'),
    path('product/case/insert/', case_insert, name='case_insert'),
    path('product/case/update/<int:id>/', case_update, name='case_update'),
    path('product/case/option/<int:id>/', case_option, name='case_option'),
    path('product/case/delete/<int:id>/', case_delete, name='case_delete'),
    # 相关产品
    path('product/relation/list/', product_relation_list, name='product_relation_list'),
    path('product/relation/insert/', product_relation_insert, name='product_relation_insert'),
    path('product/relation/update/<int:id>/', product_relation_update, name='product_relation_update'),
    path('product/relation/option/<int:id>/', product_relation_option, name='product_relation_option'),
    path('product/relation/delete/<int:id>/', product_relation_delete, name='product_relation_delete'),

    # ========== 解决方案 ==========
    # 解决方案 banner
    path('solution/banner/list/', solution_banner_list, name='solution_banner_list'),
    path('solution/banner/insert/', solution_banner_insert, name='solution_banner_insert'),
    path('solution/banner/update/<int:id>/', solution_banner_update, name='solution_banner_update'),
    # 方案背景
    path('solution/solution/list/', solution_list, name='solution_list'),
    path('solution/solution/insert/', solution_insert, name='solution_insert'),
    path('solution/solution/update/<int:id>/', solution_update, name='solution_update'),
    path('solution/solution/option/<int:id>/', solution_option, name='solution_option'),
    # 方案概述
    path('solution/summarize/list/', sol_summarize_list, name='sol_summarize_list'),
    path('solution/summarize/insert/', sol_summarize_insert, name='sol_summarize_insert'),
    path('solution/summarize/update/<int:id>/', sol_summarize_update, name='sol_summarize_update'),
    path('solution/summarize/option/<int:id>/', sol_summarize_option, name='sol_summarize_option'),
    path('solution/summarize/delete/<int:id>/', sol_summarize_delete, name='sol_summarize_delete'),
    # 应用场景
    path('solution/apply/list/', sol_apply_list, name='sol_apply_list'),
    path('solution/apply/insert/', sol_apply_insert, name='sol_apply_insert'),
    path('solution/apply/update/<int:id>/', sol_apply_update, name='sol_apply_update'),
    path('solution/apply/option/<int:id>/', sol_apply_option, name='sol_apply_option'),
    path('solution/apply/delete/<int:id>/', sol_apply_delete, name='sol_apply_delete'),
    # 方案优势
    path('solution/advantage/list/', sol_advantage_list, name='sol_advantage_list'),
    path('solution/advantage/insert/', sol_advantage_insert, name='sol_advantage_insert'),
    path('solution/advantage/update/<int:id>/', sol_advantage_update, name='sol_advantage_update'),
    path('solution/advantage/option/<int:id>/', sol_advantage_option, name='sol_advantage_option'),
    path('solution/advantage/delete/<int:id>/', sol_advantage_delete, name='sol_advantage_delete'),
    # 案例
    path('solution/case/list/', sol_case_list, name='sol_case_list'),
    path('solution/case/insert/', sol_case_insert, name='sol_case_insert'),
    path('solution/case/update/<int:id>/', sol_case_update, name='sol_case_update'),
    path('solution/case/option/<int:id>/', sol_case_option, name='sol_case_option'),
    path('solution/case/delete/<int:id>/', sol_case_delete, name='sol_case_delete'),
    # 相关产品
    path('solution/relation/list/', solution_relation_list, name='solution_relation_list'),
    path('solution/relation/insert/', solution_relation_insert, name='solution_relation_insert'),
    path('solution/relation/update/<int:id>/', solution_relation_update, name='solution_relation_update'),
    path('solution/relation/option/<int:id>/', solution_relation_option, name='solution_relation_option'),
    path('solution/relation/delete/<int:id>/', solution_relation_delete, name='solution_relation_delete'),
]
"""


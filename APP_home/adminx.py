from django.contrib import admin
import xadmin
# Register your models here.
from . import models
from xadmin import views

class GlobalSetting(object):
    site_title = 'Dataknower 后台管理系统'
    site_footer = 'dataknower.com'


xadmin.site.register(models.Swiper)
xadmin.site.register(models.Company)
xadmin.site.register(models.Home_product)
xadmin.site.register(models.Service)
xadmin.site.register(models.Client)
xadmin.site.register(views.CommAdminView, GlobalSetting)


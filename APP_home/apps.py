from django.apps import AppConfig


class AppHomeConfig(AppConfig):
    name = 'APP_home'
    verbose_name = '首页管理'

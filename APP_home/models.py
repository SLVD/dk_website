
from django.db import models


# 轮播图
from APP_header.models import PageDetail


class Swiper(models.Model):
    img = models.ImageField(upload_to='home', blank=True, null=True)
    url = models.ForeignKey(
        to=PageDetail,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name='swiper_pagedetail',
        verbose_name='链接地址',
        db_constraint=False)
    title = models.CharField(max_length=128, blank=True, null=True)
    sub_title = models.CharField(max_length=128, blank=True, null=True)
    sort = models.IntegerField(default=0, verbose_name='图片排序')
    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    update_time = models.DateTimeField(verbose_name='更新时间', auto_now=True)
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    class Meta:
        # db_table = 't_swiper'
        verbose_name = '轮播图'
        verbose_name_plural = verbose_name


# 公司简介
class Company(models.Model):
    img = models.ImageField(upload_to='home', blank=True, null=True)
    url = models.ForeignKey(
        to=PageDetail,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name='company_pagedetail',
        verbose_name='链接地址',
        db_constraint=False)
    title = models.CharField(max_length=128, blank=True, null=True)
    info = models.TextField(blank=True, null=True)
    sort = models.IntegerField(default=0, verbose_name='图片排序')
    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    update_time = models.DateTimeField(verbose_name='更新时间', auto_now=True)
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    class Meta:
        # db_table = 't_company'
        verbose_name = '公司简介'
        verbose_name_plural = verbose_name


# 核心产品
class Home_product(models.Model):
    img = models.ImageField(upload_to='home', blank=True, null=True)
    url = models.ForeignKey(
        to=PageDetail,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name='home_pro_pagedetail',
        verbose_name='链接地址',
        db_constraint=False)
    title = models.CharField(max_length=128, blank=True, null=True)
    sub_title = models.CharField(max_length=255, blank=True, null=True)
    info = models.TextField(blank=True, null=True)
    sort = models.IntegerField(default=0, verbose_name='图片排序')
    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    update_time = models.DateTimeField(verbose_name='更新时间', auto_now=True)
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    class Meta:
        # db_table = 't_service'
        verbose_name = '核心产品'
        verbose_name_plural = verbose_name



# 行业服务
class Service(models.Model):
    img = models.ImageField(upload_to='home', blank=True, null=True)
    url = models.ForeignKey(
        to=PageDetail,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name='service_pagedetail',
        verbose_name='链接地址',
        db_constraint=False)
    title = models.CharField(max_length=128, blank=True, null=True)
    info = models.TextField(blank=True, null=True)
    sort = models.IntegerField(default=0, verbose_name='图片排序')
    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    update_time = models.DateTimeField(verbose_name='更新时间', auto_now=True)
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    class Meta:
        # db_table = 't_service'
        verbose_name = '行业服务'
        verbose_name_plural = verbose_name


# 我们的客户
class Client(models.Model):
    img = models.ImageField(upload_to='home', blank=True, null=True)
    url = models.ForeignKey(
        to=PageDetail,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name='client_pagedetail',
        verbose_name='链接地址',
        db_constraint=False)
    title = models.CharField(max_length=128, blank=True, null=True)
    info = models.TextField(blank=True, null=True)
    sort = models.IntegerField(default=0, verbose_name='图片排序')
    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    update_time = models.DateTimeField(verbose_name='更新时间', auto_now=True)
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    class Meta:
        # db_table = 't_client'
        verbose_name = '我们的客户'
        verbose_name_plural = verbose_name

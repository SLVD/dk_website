//头部菜单
$(document).ready(function () {
    var scrollTop;
    "use strict";

    $('.menu > ul > li:has( > ul)').addClass('menu-dropdown-icon');

    // $(".menu > ul > li").hover(function (e) {
    $(".menu > ul").hover(function (e) {
        if ($(window).width() > 960) {
            $(".header").toggleClass("blue");

            if (!$(".menu>ul>li:first-child").hasClass('none')) {
                $(".header").addClass("blue");
            }
            e.preventDefault();
        }
    });

    $(".menu > ul > li").hover(function () {
        if ($(window).width() > 960) {
            $(this).addClass("on").children("ul").slideDown();
        }
    }, function () {
        if ($(window).width() > 960) {
            $(this).removeClass("on").children("ul").hide();
        }
    });


    $(".menu > ul > li").on("click", function (e) {
        if ($(window).width() <= 960) {
            if ($(this).hasClass('open')) {
                $('.menu > ul > li  ul').slideUp();
                $('.menu > ul > li').removeClass('open');
            } else {
                $('.menu > ul > li').removeClass('open');
                $('.menu > ul > li ul').slideUp();
                $(this).addClass('open');
                $(this).find('ul').slideDown();
            }
        }
    });

    $(window).scroll(function () {
        if ($(window).scrollTop() > 100) {
            if ($(".menu>ul>li:first-child").hasClass("none")) {
                $(".header").addClass("lineb");
            } else {
                $(".header").addClass("blue");
            }
        }
        else {
            $(".header").removeClass("lineb");
        }
        if ($(".menu > ul").css("display") === "block" && $(window).width() <= 960) {
            $(".header").removeClass("lineb");
        }
    });


    $(document).on("click", ".menu-mobile", function () {
        $(this).addClass("close");
        $(".menu > ul").fadeIn();
        $(".header").removeClass("lineb");
        scrollTop = document.scrollingElement.scrollTop;
        $("body").addClass("modal");
        document.body.style.top = -scrollTop + 'px';
    });
    $(document).on("click", ".menu-mobile.close", function () {
        $(this).removeClass("close");
        $(".menu > ul").fadeOut();
        $("body").removeClass("modal");
        document.scrollingElement.scrollTop = scrollTop;
    });

});

$(function () {
    var none = 'none';
    var flag = false; // 搜索框打开标记

    function open_searchBar() { // 打开搜索框
        $.each($(".header .menu ul>li"), function (i, v) {
            if (i > 0 && $(window).width() > 960) {
                $(v).addClass(none);
            } else {
                $(v).removeClass(none);
            }
        });

        var right = $(window).width() - $(".searchBtn").offset().left.toFixed(2) - 32;
        $(".menu > ul > li:first-child").css("right", right);
        $(".header .menu .language").css("visibility", "hidden");
        $(".searchBtn").addClass('none');
    }

    function close_searchBar() { // 关闭搜索框
        $.each($(".header .menu ul>li"), function (i, v) {
            if (i > 0 && $(window).width() > 960) {
                $(v).removeClass(none);
            } else {
                $(v).addClass(none);
            }
        });
        $(".header").removeClass('blue');
        $(".searchBtn").removeClass('none');
        $(".searchBar input").val('');
        $(".header .menu .language").css("visibility", "visible");
    };

    // 打开搜索框
    $(".searchBtn").on("click", function () {
        flag = true;
        open_searchBar();
    });

    // 关闭搜索框 / 清空搜索框
    $(".header .menu .searchBar .close").on("click", function () {
        $(".searchBar input").val('');
    });

    // 打开后，点击其它div关闭搜索框
    $(document).on("click", function (e) {
        e = window.event || e;
        var that = $(e.srcElement || e.target); // 当前点击目标
        var thats = ""; // // 当前点击目标的header级别元素
        var thatValue = that.attr('class') || '';
        if (that != undefined) {
            var _that = that.parents();
            thats = $(_that[_that.length - 3]).attr('class') || '';
        }
        if (thatValue === 'serIco') {
            return false;
        } else if (flag && !(thatValue.indexOf('header') > -1 || thats.indexOf('header') > -1)) {
            flag = false;
            close_searchBar();
        }
    });

    //点击搜索
    $(".searchIco").on("click", function () {
        var txt = $(".header .searchBar input[type='text']").val();
        if (txt.length < 1) {
            layer.alert('您输入的内容不能为空!');
            return false;
        }
    });

    $(window).resize(function () {
        close_searchBar();
    });

});



$(function () {
    var wow = new WOW({
        animateClass: 'animated',
        offset: 0
    });
    wow.init();

    var winw = $(window).innerWidth();
    $('.index-ban').css('height', winw * 833 / 1600)
    $(window).resize(function () {
        winw = $(window).innerWidth();
        $('.index-ban').css('height', winw * 833 / 1600)
    })

    var swiper1 = new Swiper('.swiper-container1', {
        autoplay: {
            delay: 5000,
            stopOnLastSlide: false,
            disableOnInteraction: false,
        },
        pagination: {
            el: '.swiper-pagination1',
            clickable: true,
        },
    });

    var swiper2 = new Swiper('.swiper-container2', {
        autoplay: {
            delay: 5000,
            stopOnLastSlide: false,
            disableOnInteraction: false,
        },
        pagination: {
            el: '.swiper-pagination2',
            clickable: true,
        },
    });
});


$(function () {
    var pathname = window.location.pathname;
    var active = 'active';
    var path = '/'
    if ( pathname.length != 1 ) {
        path = pathname.substring(0, pathname.length - 1);
    }
    var that = $(".menu > ul > li > a[href='"+ path +"']");
    that.addClass(active)
});
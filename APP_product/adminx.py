from django.contrib import admin
import xadmin
# Register your models here.
from . import models

xadmin.site.register(models.Product_banner)
xadmin.site.register(models.Product)
xadmin.site.register(models.Function)
xadmin.site.register(models.Advantage)
xadmin.site.register(models.Apply)
xadmin.site.register(models.Case)
xadmin.site.register(models.Product_relation)

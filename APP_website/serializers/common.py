from rest_framework import serializers

from APP_header.models import *


class HeaderSerializer(serializers.ModelSerializer):
    create_time = serializers.SerializerMethodField()
    def get_create_time(self, obj):
        return obj.create_time.strftime("%Y-%m-%d %H:%I:%S")

    class Meta:
        model = Header_admin
        fields = '__all__'





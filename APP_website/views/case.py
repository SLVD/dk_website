import json

from django.forms import model_to_dict
from django.shortcuts import render
from APP_case.models import *
from django.conf import settings
MEDIA_URL = settings.MEDIA_URL


def banner():
    result = {}
    data_list = Case_banner.objects.filter(status=1)
    for _o in data_list:
        result.update({
            "img": MEDIA_URL + str(_o.img),
            "url": _o.url.url,
            "title": _o.title,
            "info": _o.info,
        })
    return result


def case():
    result = []
    data_list = Case.objects.filter(status=1)
    for _o in data_list:
        result.append({
            "img": MEDIA_URL + str(_o.img),
            "url": _o.url.url,
            "title": _o.title,
            "info": _o.info,
        })
    return result


def case_data(request):
    if request.method == 'GET':
        context = {}
        context['banner'] = banner()
        context['case'] = case()
        return render(request, 'case.html', context)


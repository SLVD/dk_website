import json

from django.forms import model_to_dict
from django.shortcuts import render
from APP_solution.models import *
from django.conf import settings
MEDIA_URL = settings.MEDIA_URL

def banner():
    result = {}
    data_list = Solution_banner.objects.filter(status=1)
    for _o in data_list:
        result.update({
            "img": MEDIA_URL + str(_o.img),
            "url": _o.url.url,
            "title": _o.title,
            "info": _o.info,
        })
    return result


def solution():
    result = {}
    data_list = Solution.objects.filter(status=1)
    for _o in data_list:
        result.update({
            "img": MEDIA_URL + str(_o.img),
            "url": _o.url.url,
            "title": _o.title,
            "info": _o.info,
        })
    return result


def summarize():
    result = {}
    data_list = Sol_summarize.objects.filter(status=1)
    for _o in data_list:
        result.update({
            "img": MEDIA_URL + str(_o.img),
            "url": _o.url.url,
            "title": _o.title,
            "info": _o.info,
            "sort": _o.sort,
        })
    return result


def advantage():
    result = []
    data_list = Sol_advantage.objects.filter(status=1).order_by('sort')
    for _o in data_list:
        result.append({
            "img": MEDIA_URL + str(_o.img),
            "url": _o.url.url,
            "title": _o.title,
            "info": _o.info,
            "sort": _o.sort,
        })
    return result


def apply():
    result = []
    data_list = Sol_apply.objects.filter(status=1).order_by('sort')
    for _o in data_list:
        result.append({
            "img": MEDIA_URL + str(_o.img),
            "url": _o.url.url,
            "title": _o.title,
            "info": _o.info,
            "sort": _o.sort,
        })
    return result


def case():
    result = []
    data_list = Sol_case.objects.filter(status=1).order_by('sort')
    for _o in data_list:
        result.append({
            "img": MEDIA_URL + str(_o.img),
            "url": _o.url.url,
            "title": _o.title,
            "info": _o.info,
            "sort": _o.sort,
        })
    return result


def relation():
    result = []
    data_list = Solution_relation.objects.filter(status=1).order_by('sort')
    for _o in data_list:
        result.append({
            "img": MEDIA_URL + str(_o.img),
            "url": _o.url.url,
            "title": _o.title,
            "info": _o.info,
            "sort": _o.sort,
        })
    return result


def solution_data(request):
    if request.method == 'GET':
        context = {}
        context['banner'] = banner()
        context['solution'] = solution()
        context['summarize'] = summarize()
        context['advantage'] = advantage()
        context['apply'] = apply()
        context['case'] = case()
        context['relation'] = relation()
        return render(request, 'solution.html', context)


import json

from django.forms import model_to_dict
from django.shortcuts import render
from APP_company.models import *
from django.conf import settings
from APP_header.models import PageDetail

MEDIA_URL = settings.MEDIA_URL


def detail_data(request, id):
    if request.method == 'GET':
        data = PageDetail.objects.filter(id=id).first()
        context = {}
        context['detail'] = model_to_dict(data)
        print('cont', context)
        return render(request, 'detail.html', context)


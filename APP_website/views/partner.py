import json

from django.forms import model_to_dict
from django.shortcuts import render
from APP_partner.models import *
from django.conf import settings
MEDIA_URL = settings.MEDIA_URL

def banner():
    result = {}
    data_list = Partner_banner.objects.filter(status=1)
    for _o in data_list:
        result.update({
            "img": MEDIA_URL + str(_o.img),
            "url": _o.url.url,
            "title": _o.title,
            "info": _o.info,
        })
    return result


def strategic():
    result = []
    data_list = Strategic.objects.filter(status=1)
    for _o in data_list:
        result.append({
            "img": MEDIA_URL + str(_o.img),
            "url": _o.url.url,
            "title": _o.title,
            "info": _o.info,
        })
    return result


def channel():
    result = []
    data_list = Channel.objects.filter(status=1)
    for _o in data_list:
        result.append({
            "img": MEDIA_URL + str(_o.img),
            "url": _o.url.url,
            "title": _o.title,
            "info": _o.info,
            "sort": _o.sort,
        })
    return result


def process():
    result = []
    data_list = Process.objects.filter(status=1).order_by('sort')
    for _o in data_list:
        result.append({
            "img": MEDIA_URL + str(_o.img),
            "url": _o.url.url,
            "title": _o.title,
            "info": _o.info,
            "sort": _o.sort,
        })
    return result


def interests():
    result = []
    data_list = Interests.objects.filter(status=1).order_by('sort')
    for _o in data_list:
        result.append({
            "img": MEDIA_URL + str(_o.img),
            "url": _o.url.url,
            "title": _o.title,
            "info": _o.info,
            "sort": _o.sort,
        })
    return result


def partner_data(request):
    if request.method == 'GET':
        context = {}
        context['banner'] = banner()
        context['strategic'] = strategic()
        context['channel'] = channel()
        context['process'] = process()
        context['interests'] = interests()
        return render(request, 'partner.html', context)


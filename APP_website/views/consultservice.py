import json

from django.forms import model_to_dict
from django.shortcuts import render
from APP_consultservice.models import *
from django.conf import settings
MEDIA_URL = settings.MEDIA_URL

def banner():
    result = {}
    data_list = Consult_banner.objects.filter(status=1)
    for _o in data_list:
        result.update({
            "img": MEDIA_URL + str(_o.img),
            "url": _o.url.url,
            "title": _o.title,
            "info": _o.info,
        })
    return result

# 大数据战略规划
def consuleservice():
    result = []
    data_list = ConsuleService.objects.filter(status=1)
    for _o in data_list:
        result.append({
            "img": MEDIA_URL + str(_o.img),
            "url": _o.url.url,
            "title": _o.title,
            "info": _o.info,
        })
    return result


# 数据运营咨询
def dataOperation():
    result = []
    data_list = DataOperation.objects.filter(status=1)
    for _o in data_list:
        result.append({
            "img": MEDIA_URL + str(_o.img),
            "url": _o.url.url,
            "title": _o.title,
            "info": _o.info,
            "sort": _o.sort,
        })
    return result


# 数据科学建模
def dataScience():
    result = []
    data_list = DataScience.objects.filter(status=1).order_by('sort')
    for _o in data_list:
        result.append({
            "img": MEDIA_URL + str(_o.img),
            "url": _o.url.url,
            "title": _o.title,
            "info": _o.info,
            "sort": _o.sort,
        })
    return result


# 商业分析报告
def analysisReport():
    result = []
    data_list = AnalysisReport.objects.filter(status=1).order_by('sort')
    for _o in data_list:
        result.append({
            "img": MEDIA_URL + str(_o.img),
            "url": _o.url.url,
            "title": _o.title,
            "info": _o.info,
            "sort": _o.sort,
        })
    return result


def consultservice_data(request):
    if request.method == 'GET':
        context = {}
        context['banner'] = banner()
        context['consuleservice'] = consuleservice()
        context['dataOperation'] = dataOperation()
        context['dataScience'] = dataScience()
        context['analysisReport'] = analysisReport()
        return render(request, 'consult.service.html', context)


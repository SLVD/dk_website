import json

from django.forms import model_to_dict
from django.shortcuts import render
from APP_company.models import *
from django.conf import settings
MEDIA_URL = settings.MEDIA_URL


def banner():
    result = {}
    data_list = Company_banner.objects.filter(status=1)
    for _o in data_list:
        result.update({
            "img": MEDIA_URL + str(_o.img),
            "url": _o.url.url,
            "title": _o.title,
            "info": _o.info,
        })
    return result


def company():
    result = {}
    data_list = Company.objects.filter(status=1)
    for _o in data_list:
        result.update({
            "img": MEDIA_URL + str(_o.img),
            "url": _o.url.url,
            "title": _o.title,
            "info": _o.info,
        })
    return result


def innovation():
    result = []
    data_list = Innovation.objects.filter(status=1).order_by('sort')
    for _o in data_list:
        result.append({
            "img": MEDIA_URL + str(_o.img),
            "url": _o.url.url,
            "title": _o.title,
            "info": _o.info,
            "sort": _o.sort,
        })
    return result


def layout():
    result = {}
    data_list = Layout.objects.filter(status=1)
    for _o in data_list:
        result.update({
            "img": MEDIA_URL + str(_o.img),
            "url": _o.url.url,
            "title": _o.title,
            "info": _o.info,
        })
    return result


def honor():
    result = {}
    data_list = Honor.objects.filter(status=1)
    for _o in data_list:
        result.update({
            "img": MEDIA_URL + str(_o.img),
            "url": _o.url.url,
            "title": _o.title,
            "info": _o.info,
        })
    return result


def company_data(request):
    if request.method == 'GET':
        context = {}
        context['banner'] = banner()
        context['company'] = company()
        context['innovation'] = innovation()
        context['layout'] = layout()
        context['honor'] = honor()
        return render(request, 'company.html', context)


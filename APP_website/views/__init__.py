from .home import *
from .product import *
from .solution import *
from .company import *
from .consultservice import *
from .partner import *
from .case import *
from .detail import *
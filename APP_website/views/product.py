from django.shortcuts import render
from APP_product.models import *
from django.conf import settings
MEDIA_URL = settings.MEDIA_URL


def banner():
    result = {}
    data_list = Product_banner.objects.filter(status=1)
    for _o in data_list:
        result.update({
            "img": MEDIA_URL + str(_o.img),
            "url": _o.url.url,
            "title": _o.title,
            "info": _o.info,
        })
    return result


def product():
    result = {}
    data_list = Product.objects.filter(status=1)
    for _o in data_list:
        result.update({
            "img": MEDIA_URL + str(_o.img),
            "url": _o.url.url,
            "title": _o.title,
            "info": _o.info,
        })
    return result


def function():
    result = []
    data_list = Function.objects.filter(status=1)
    for _o in data_list:
        result.append({
            "img": MEDIA_URL + str(_o.img),
            "url": _o.url.url,
            "title": _o.title,
            "info": _o.info,
            "sort": _o.sort,
        })
    return result


def advantage():
    result = []
    data_list = Advantage.objects.filter(status=1).order_by('sort')
    for _o in data_list:
        result.append({
            "img": MEDIA_URL + str(_o.img),
            "url": _o.url.url,
            "title": _o.title,
            "info": _o.info,
            "sort": _o.sort,
        })
    return result


def apply():
    result = []
    data_list = Apply.objects.filter(status=1).order_by('sort')
    for _o in data_list:
        result.append({
            "img": MEDIA_URL + str(_o.img),
            "url": _o.url.url,
            "title": _o.title,
            "info": _o.info,
            "sort": _o.sort,
        })
    return result


def case():
    result = []
    data_list = Case.objects.filter(status=1).order_by('sort')
    for _o in data_list:
        result.append({
            "img": MEDIA_URL + str(_o.img),
            "url": _o.url.url,
            "title": _o.title,
            "info": _o.info,
            "sort": _o.sort,
        })
    return result


def relation():
    result = []
    data_list = Product_relation.objects.filter(status=1).order_by('sort')
    for _o in data_list:
        result.append({
            "img": MEDIA_URL + str(_o.img),
            "url": _o.url.url,
            "title": _o.title,
            "info": _o.info,
            "sort": _o.sort,
        })
    return result


def product_data(request):
    if request.method == 'GET':
        context = {}
        context['banner'] = banner()
        context['product'] = product()
        context['function'] = function()
        context['advantage'] = advantage()
        context['apply'] = apply()
        context['case'] = case()
        context['relation'] = relation()
        return render(request, 'product.html', context)


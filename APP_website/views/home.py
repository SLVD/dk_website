import json

from django.forms import model_to_dict
from django.shortcuts import render
from APP_home.models import *
from django.conf import settings
MEDIA_URL = settings.MEDIA_URL

def swiper():
    result = []
    data_list = Swiper.objects.filter(status=1).order_by('sort')
    for _o in data_list:
        url = ''
        # if _o.url:
            # url = _o.url["url"]
        result.append({
            "sort": _o.sort,
            "img": MEDIA_URL + str(_o.img),
            # "img": _o.img,
            "url": url,
            "title": _o.title,
            "sub_title": _o.sub_title,
        })
    return result


def company():
    result = []
    data_list = Company.objects.filter(status=1).order_by('sort')
    url = "javascript:;"
    for _o in data_list:
        print('====--==', _o.url)
        if _o.url:
            url = _o.url.url
        result.append({
            "sort": _o.sort,
            "img": MEDIA_URL + str(_o.img),
            # "img": _o.img,
            "url": url,
            "title": _o.title,
            "info": _o.info,
        })
    print('pro', result)
    return result


def home_product():
    result = []
    data_list = Home_product.objects.filter(status=1).order_by('sort')
    for _o in data_list:
        result.append({
            "img": MEDIA_URL + str(_o.img),
            "url": _o.url.url,
            "title": _o.title,
            "info": _o.info,
        })

    print('abc', result)
    return result


def service():
    result = []
    data_list = Service.objects.filter(status=1).order_by('sort')
    for _o in data_list:
        result.append({
            "img": MEDIA_URL + str(_o.img),
            "url": _o.url.url,
            "title": _o.title,
            "info": _o.info,
            "sort": _o.sort,
        })
    return result


def client():
    result = []
    data_list = Client.objects.filter(status=1).order_by('sort')
    for _o in data_list:
        result.append({
            "img": MEDIA_URL + str(_o.img),
            "url": _o.url.url,
            "title": _o.title,
            "info": _o.info,
            "sort": _o.sort,
        })
    return result


def home_data(request):
    if request.method == 'GET':
        context = {}
        context['img'] = 'images/lenovo.png'
        context['swiper'] = swiper()
        context['company'] = company()
        context['home_product'] = home_product()
        context['service'] = service()
        context['client'] = client()
        print('home', context)
        return render(request, 'home.html', context)


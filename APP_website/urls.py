from django.urls import path
from .views import *


urlpatterns = [
    path('', home_data, name='home_data'),
    path('product/', product_data, name='product_data'),
    path('solution/', solution_data, name='solution_data'),
    path('consultservice/', consultservice_data, name='consultservice_data'),
    path('case/', case_data, name='case_data'),
    path('partner/', partner_data, name='partner_data'),
    path('company/', company_data, name='company_data'),
    path('detail/<int:id>/', detail_data, name='detail_data'),

]


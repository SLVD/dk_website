import os
from crispy_forms import templatetags
import sys

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
root = lambda *x: os.path.join(BASE_DIR, *x)

sys.path.append(os.path.join(BASE_DIR, 'extra_apps'))

SECRET_KEY = 'th3%n%8j0vcu$i+j%r*=(=5e^#e%)sp(u88djf9!czgozt&rh8'

DEBUG = True

ALLOWED_HOSTS = ['*']

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'xadmin',
    'crispy_forms',
]

PROJECT_APP = [
    'APP_test',
    'APP_website',
    'APP_home',
    'APP_product',
    'APP_solution',
    'APP_consultservice',
    'APP_case',
    'APP_partner',
    'APP_company',
    'APP_header',
    'APP_footer',
    'extra_apps.DjangoUeditor',
]

INSTALLED_APPS += PROJECT_APP

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    # 'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'dk_website.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        # 'DIRS': [],
        'DIRS': [BASE_DIR+'/templates'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
            'builtins': [
                'django.templatetags.static'
            ],
            'libraries': {
                'mytet': 'templatetags.mytet',
                'head': 'templatetags.head',
                'header': 'templatetags.header',
                'footer': 'templatetags.footer',
            }
        },
    },
]

WSGI_APPLICATION = 'dk_website.wsgi.application'

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


LANGUAGE_CODE = 'zh-Hans'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATIC_URL = '/static/'

STATIC_ROOT = os.path.join(BASE_DIR, 'collectstatic')

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
)

STATIC_DOMAIN = '/server'

PREFIX_URL = 'https://'

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'

_DEFAULT_FILE_STORAGE = 'qiniustorage.backends.QiniuMediaStorage_'
_STATICFILES_STORAGE = 'qiniustorage.backends.QiniuStaticStorage_'

QINIU_ACCESS_KEY = 'Z2qvhrrcvWb2pMqcCJqfJ4KN_XA0KYuDuPwMdhNm'
QINIU_SECRET_KEY = '7lqqrfSmduOGi1mEcsZUjchXdXIVRKVkKXfzuc3M'
QINIU_BUCKET_NAME = 'fenjian'
QINIU_BUCKET_DOMAIN = 'qiniu.digfunny.com'
QINIU_SECURE_URL = True

if _STATICFILES_STORAGE == 'qiniustorage.backends.QiniuStaticStorage':
    # 七牛存储
    STATIC_URL = PREFIX_URL + QINIU_BUCKET_DOMAIN + '/collectstatic/'
    STATIC_ROOT = 'collectstatic'
    STATICFILES_STORAGE = _STATICFILES_STORAGE
else:
    # 本地存储
    STATIC_URL = '/static/'
    STATIC_ROOT = root('collectstatic')

if _DEFAULT_FILE_STORAGE == 'qiniustorage.backends.QiniuMediaStorage':
    # 七牛上传
    MEDIA_URL = PREFIX_URL + QINIU_BUCKET_DOMAIN + '/'
    MEDIA_ROOT = 'media'
    DEFAULT_FILE_STORAGE = _DEFAULT_FILE_STORAGE
else:
    # 本地上传
    MEDIA_URL = '/media/'
    MEDIA_ROOT = root('media')

with open(os.path.join(BASE_DIR, 'ENV'), 'r') as f:
    env = f.readline().strip()
    if env == 'dev':
        from .dev import *
    elif env == 'test':
        from .test import *
    elif env == 'pro':
        from .pro import *


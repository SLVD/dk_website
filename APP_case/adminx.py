from django.contrib import admin
import xadmin
# Register your models here.
from . import models

xadmin.site.register(models.Case_banner)
xadmin.site.register(models.Case)

from django.db import models

# Create your models here.


# 案例 banner
from APP_header.models import PageDetail


class Case_banner(models.Model):
    img = models.ImageField(upload_to='solution', blank=True, null=True)
    url = models.ForeignKey(
        to=PageDetail,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        verbose_name='链接地址',
        db_constraint=False)
    title = models.CharField(max_length=128, blank=True, null=True)
    info = models.TextField(blank=True, null=True)
    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    update_time = models.DateTimeField(verbose_name='更新时间', auto_now=True)
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    class Meta:
        # db_table = 't_solution_banner'
        verbose_name = '案例 banner'
        verbose_name_plural = verbose_name


# 案例
class Case(models.Model):
    img = models.ImageField(upload_to='solution', blank=True, null=True)
    url = models.ForeignKey(
        to=PageDetail,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name="case_case",
        verbose_name='链接地址',
        db_constraint=False)
    title = models.CharField(max_length=128, blank=True, null=True)
    info = models.TextField(blank=True, null=True)
    sort = models.IntegerField(default=0, verbose_name='图片排序')
    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    update_time = models.DateTimeField(verbose_name='更新时间', auto_now=True)
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    class Meta:
        # db_table = 't_sol_apply'
        verbose_name = '案例'
        verbose_name_plural = verbose_name


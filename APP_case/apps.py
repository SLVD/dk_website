from django.apps import AppConfig


class AppCaseConfig(AppConfig):
    name = 'APP_case'
    verbose_name = '案例'

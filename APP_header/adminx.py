from django.contrib import admin
import xadmin
# Register your models here.
from . import models


class HeaderAdminView(object):
    list_display = ['title', 'pid', 'sort', 'status']
    ordering = ('sort',)

    # def queryset(self):
    #     qs = super(HeaderAdminView, self).queryset()
    #     qs = qs.filter().order_by('sort')
    #     return qs


class PageDetailView(object):
    style_fields = {'info': 'ueditor'}
    list_display = ['title', 'url', 'type']
    ordering = ('type',)


# xadmin.site.register(models.Header)
xadmin.site.register(models.PageDetail, PageDetailView)
xadmin.site.register(models.Header_admin, HeaderAdminView)


from django.apps import AppConfig


class AppHeaderConfig(AppConfig):
    name = 'APP_header'
    verbose_name = '导航管理'

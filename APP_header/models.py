from django.db import models
# Create your models here.


# 导航管理
from DjangoUeditor.models import UEditorField
from django.template.defaultfilters import striptags


class Header(models.Model):
    url = models.CharField(max_length=255, blank=True, null=True)
    title = models.CharField(max_length=128, blank=True, null=True)
    info = models.CharField(max_length=128, blank=True, null=True)
    sort = models.IntegerField(default=0, verbose_name='图片排序')
    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    update_time = models.DateTimeField(verbose_name='更新时间', auto_now=True)
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.title + str(self.sort)

    class Meta:
        # db_table = 't_solution_banner'
        verbose_name = '导航管理'
        verbose_name_plural = verbose_name


# 页面详情
class PageDetail(models.Model):
    type_choice = [
        (0, '详情'),
        (1, '导航'),
    ]
    title = models.CharField(max_length=128, blank=True, null=True)
    info = UEditorField(verbose_name='详情', width=600, height=400, imagePath='ueditor/', filePath='ueditor/',
                        default='', null=True, blank=True)
    url = models.CharField(max_length=255, blank=True, null=True, )
    type = models.IntegerField(default=0, choices=type_choice, verbose_name="类型")
    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    update_time = models.DateTimeField(verbose_name='更新时间', auto_now=True)
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        super(PageDetail, self).save(args, kwargs)
        if self.type == 0:
            self.url = '/detail/' + str(self.id)
        elif self.type == 1:
            self.url = striptags(self.info)
        super(PageDetail, self).save(args, kwargs)

    class Meta:
        # db_table = 't_solution_banner'
        verbose_name = '页面详情'
        verbose_name_plural = verbose_name


# 导航管理 -- 多级
class Header_admin(models.Model):
    url = models.ForeignKey(
        to=PageDetail,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        verbose_name='链接地址',
        db_constraint=False)
    title = models.CharField(max_length=128, blank=True, null=True)
    pid = models.ForeignKey(
        to='self',
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        verbose_name='父级菜单',
        db_constraint=False)
    sort = models.IntegerField(default=0, verbose_name='排序')
    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    update_time = models.DateTimeField(verbose_name='更新时间', auto_now=True)
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    class Meta:
        # db_table = 't_solution_banner'
        verbose_name = '导航管理'
        verbose_name_plural = verbose_name

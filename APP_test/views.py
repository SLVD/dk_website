from django.forms import model_to_dict
from django import template
from django.shortcuts import render
from basic.basic_response import jsonp_wrapped_response, jsonp_res_data, DataPackage
from basic.basic_response_result import MSG, CODE
from qiniu import Auth
from APP_test.models import *

# Create your views here.
def test(request):
    data_package = DataPackage().set_fields({
        'msg': 'SUCCESS'
    })
    return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T'], data=data_package))

# qiniu token
def upload_qiniu(request):
    access_key = 'Z2qvhrrcvWb2pMqcCJqfJ4KN_XA0KYuDuPwMdhNm'
    secret_key = '7lqqrfSmduOGi1mEcsZUjchXdXIVRKVkKXfzuc3M'
    domain = 'qiniu.sudidi.cn'
    # 构建鉴权对象
    q = Auth(access_key, secret_key)
    # 要上传的空间
    bucket_name = 'dk-cake'
    # 上传后保存的文件名
    key = None
    # 生成上传 Token，可以指定过期时间等
    # 上传策略示例
    # https://developer.qiniu.com/kodo/manual/1206/put-policy
    policy = {
        # 'callbackUrl':'https://requestb.in/1c7q2d31',
        # 'callbackBody':'filename=$(fname)&filesize=$(fsize)'
        # 'persistentOps':'imageView2/1/w/200/h/200'
    }
    # 3600为token过期时间，秒为单位。3600等于一小时
    token = q.upload_token(bucket_name, key, 3600 * 24 * 30, policy)
    # token = q.upload_token(bucket_name)
    # print(token)
    data_package = DataPackage().set_fields({
        'token': token,
        'domain': domain
    })
    return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T'], data=data_package))


def add_test(request):
    obj = Test.objects.create(
        name='xixi',
    )
    result = {
        'obj': model_to_dict(obj)
    }
    data_package = DataPackage().set_fields(result)
    return jsonp_wrapped_response(jsonp_res_data(msg='T', rc=0, data=data_package))


def hello(request):
    context = {}
    context['hello'] = 'Hello World!'
    context['hello2'] = 'Hello World!'
    context['img'] = 'images/lenovo.png'
    return render(request, 'home.html', context)


register = template.Library()
@register.inclusion_tag('t.html')
def task_l():
    return {'task': [{'aa': 11}, {'bb': 22}, {'cc': 33}]}

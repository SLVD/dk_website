from django.db import models

# Create your models here.
class Test(models.Model):
    name = models.CharField(max_length=128, blank=True, null=True)
    status = models.BooleanField(default=True)

    class Meta:
        db_table = 't_test'
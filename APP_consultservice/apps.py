from django.apps import AppConfig


class AppConsultserviceConfig(AppConfig):
    name = 'APP_consultservice'
    verbose_name = '咨询服务'

from django.contrib import admin
import xadmin
# Register your models here.
from . import models

xadmin.site.register(models.Consult_banner)
xadmin.site.register(models.ConsuleService)
xadmin.site.register(models.DataOperation)
xadmin.site.register(models.DataScience)
xadmin.site.register(models.AnalysisReport)

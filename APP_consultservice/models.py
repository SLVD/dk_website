from django.db import models

# Create your models here.


# 咨询服务 banner
from APP_header.models import PageDetail


class Consult_banner(models.Model):
    img = models.ImageField(upload_to='solution', blank=True, null=True)
    url = models.ForeignKey(
        to=PageDetail,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        verbose_name='链接地址',
        db_constraint=False)
    title = models.CharField(max_length=128, blank=True, null=True)
    info = models.TextField(blank=True, null=True)
    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    update_time = models.DateTimeField(verbose_name='更新时间', auto_now=True)
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    class Meta:
        # db_table = 't_solution_banner'
        verbose_name = '咨询服务 banner'
        verbose_name_plural = verbose_name


# 大数据战略规划
class ConsuleService(models.Model):
    img = models.ImageField(upload_to='solution', blank=True, null=True)
    url = models.ForeignKey(
        to=PageDetail,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        verbose_name='链接地址',
        db_constraint=False)
    title = models.CharField(max_length=128, blank=True, null=True)
    info = models.TextField(blank=True, null=True)
    sort = models.IntegerField(default=0, verbose_name='图片排序')
    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    update_time = models.DateTimeField(verbose_name='更新时间', auto_now=True)
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    class Meta:
        # db_table = 't_solution'
        verbose_name = '大数据战略规划'
        verbose_name_plural = verbose_name


# 数据运营咨询
class DataOperation (models.Model):
    img = models.ImageField(upload_to='solution', blank=True, null=True)
    url = models.ForeignKey(
        to=PageDetail,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        verbose_name='链接地址',
        db_constraint=False)
    title = models.CharField(max_length=128, blank=True, null=True)
    info = models.TextField(blank=True, null=True)
    sort = models.IntegerField(default=0, verbose_name='图片排序')
    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    update_time = models.DateTimeField(verbose_name='更新时间', auto_now=True)
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    class Meta:
        # db_table = 't_sol_summarize'
        verbose_name = '数据运营咨询'
        verbose_name_plural = verbose_name


# 数据科学建模
class DataScience(models.Model):
    img = models.ImageField(upload_to='solution', blank=True, null=True)
    url = models.ForeignKey(
        to=PageDetail,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        verbose_name='链接地址',
        db_constraint=False)
    title = models.CharField(max_length=128, blank=True, null=True)
    info = models.TextField(blank=True, null=True)
    sort = models.IntegerField(default=0, verbose_name='图片排序')
    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    update_time = models.DateTimeField(verbose_name='更新时间', auto_now=True)
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    class Meta:
        # db_table = 't_sol_advantage'
        verbose_name = '数据科学建模'
        verbose_name_plural = verbose_name


# 商业分析报告
class AnalysisReport(models.Model):
    img = models.ImageField(upload_to='solution', blank=True, null=True)
    url = models.ForeignKey(
        to=PageDetail,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        verbose_name='链接地址',
        db_constraint=False)
    title = models.CharField(max_length=128, blank=True, null=True)
    info = models.TextField(blank=True, null=True)
    sort = models.IntegerField(default=0, verbose_name='图片排序')
    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    update_time = models.DateTimeField(verbose_name='更新时间', auto_now=True)
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    class Meta:
        # db_table = 't_sol_apply'
        verbose_name = '商业分析报告'
        verbose_name_plural = verbose_name
